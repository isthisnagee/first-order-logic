# first-order-logic

This is a member of
[dum-stuff-4-fun](https://isthisnagee.github.io/dum-stuff-for-fun),
a list of projects that may or may not have any value. It's my portfolio of
non-professional projects (usually with weeeeeird code).

I'm going to try to model first order logic in code, and I want to see if I
can make an interactive "game" that teaches it. I'm just going wherever this
takes me.
